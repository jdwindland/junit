package com.jdwindland;

import java.lang.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    //Method to create an array to for the assertArrayEquals test.
    public static String[] familyMembers(){
        String[] family = {"Joseph","Jenny","Kayte","Andy","Emily","Trelynd"};
        for (int i = 0; i < family.length; i++){
            System.out.println(family[i]);
        }
        return family;
    }

    //Method to create a map for the assertNotSame test.
    public static String familyGender(final String a){
        Map<String, String> gender = new LinkedHashMap<String, String>();
        gender.put("Joseph", "Male");
        gender.put("Jenny", "Female");
        gender.put("Kayte", "Female");
        gender.put("Andy", "Male");
        gender.put("Emily", "Female");
        gender.put("Trelynd", "Male");
        System.out.println(gender);
        return gender.get(a);
    }

    //Method to create a map for the assertNotNull test.
    public static String familyAge(final String a){
        Map<String, String> age = new LinkedHashMap<String, String>();
        age.put("Joseph", "55");
        age.put("Jenny", "44");
        age.put("Kayte", "25");
        age.put("Andy", "25");
        age.put("Emily", "24");
        age.put("Trelynd", "23");
        System.out.println(age);
        return age.get(a);
    }

    //Method to create a map for the assertSame test.
    public static String familyCity(final String a){
        Map<String, String> city = new LinkedHashMap<String, String>();
        city.put("Joseph", "Deer Creek, IL");
        city.put("Jenny", "Deer Creek, IL");
        city.put("Kayte", "Morton, IL");
        city.put("Andy", "Tuscon, AZ");
        city.put("Emily", "Deer Creek, IL");
        city.put("Trelynd", "Tempe, AZ");
        System.out.println(city);
        return city.get(a);
    }

    //Method to create a map for the assertNull test.
    public static String familyColor(final String a){
        Map<String, String> color = new LinkedHashMap<String, String>();
        color.put("Joseph", "Red");
        color.put("Jenny", "Orange");
        color.put("Andy", "Blue");
        color.put("Trelynd", "Green");
        System.out.println(color);
        return color.get(a);
    }

    //Method with output for the assertEquals test.
    public static int multiply(int a, int b){
        return a * b;
    }

    //Method for the assertFalse test.
    public static boolean evenNumber(int a){
        boolean num1 = false;
        if(a%2 == 0){
            num1 = true;
        }
        return num1;
    }

    //Method for the assertTrue test.
    public static boolean oddNumber(int a){
        boolean num2 = true;
        if(a%2 == 0){
            num2 = false;
        }
        return num2;
    }

    //Main method to run the program.
    public static void main(String[] args) {

        //Instantiate the Scanner object for data entry.
        Scanner systemInScanner = new Scanner (System.in);

        //Prints out the array of family members.
        System.out.println("The members of my family are:");
        familyMembers();
        System.out.println();

        //Prints the map for gender.
        System.out.println("Their gender is:");
        familyGender("Joseph");
        System.out.println();

        //Prints the map for age.
        System.out.println("Their ages are:");
        familyAge("Joseph");
        System.out.println();

        //Prints the map for city.
        System.out.println("They live in:");
        familyCity("Joseph");
        System.out.println();

        //Prints the map for color.
        System.out.println("Their favorite color is:");
        familyColor("Joseph");
        System.out.println();

        /*
        Allows user to enter two numbers and multiplies them together.
        Data validation to ensure only numbers are entered.
         */
        System.out.println("Let's multiply some numbers.");
        int num;
        int num1;
        int num2;
        do {
            System.out.print("Please enter a number. ");
            while (!systemInScanner.hasNextInt()) {
                String input1 = systemInScanner.next();
                System.out.println(input1 + " is not a number.\n" + input1);
            }
            num1 = systemInScanner.nextInt();
        }while (num1<0);
        do {
            System.out.print("Please enter a second number. ");
            while (!systemInScanner.hasNextInt()) {
                String input2 = systemInScanner.next();
                System.out.println(input2 + " is not a number.\n" + input2);
            }
            num2 = systemInScanner.nextInt();
        }while (num2 < 0);
        num = multiply(num1, num2);
        System.out.println(num1 + " and " + num2 + " multiplied together equals: " + num);
        System.out.println();

        /*
        Allows user to enter a number tells them if it is an even number.
        Data validation to ensure only numbers are entered.
         */
        String result1;
        int num3;
        do{
            System.out.print("Enter an even number: ");
            while (!systemInScanner.hasNextInt()){
                String input3 = systemInScanner.next();
                System.out.println(input3 + " is not a number.\n");
            }
            num3 = systemInScanner.nextInt();
        } while (num3 < 0);
        if(evenNumber(num3) == true){
            result1 = "Yes";
        } else result1 = "No";
        System.out.println("Is " + num3 + " an even number? " + result1);
        System.out.println();

        /*
        Allows user to enter a number tells them if it is an odd number.
        Data validation to ensure only numbers are entered.
         */
        String result2;
        int num4;
        do{
            System.out.print("Enter an odd number: ");
            while(!systemInScanner.hasNextInt()){
                String input4 = systemInScanner.next();
                System.out.println(input4 + "is not a number.\n");
            }
            num4 = systemInScanner.nextInt();
        }while (num4 < 0);
        if(oddNumber(num4) == true){
            result2 = "Yes";
        } else result2 = "No";
        System.out.println("Is " + num4 + " an odd number? " + result2);
        System.out.println();
    }
}
