import com.jdwindland.Main;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestCases {

    @Test
    //Test to verify array matches what was expected.
    public void testFamilyMembers() {
        String[] expected = {"Joseph","Jenny","Kayte","Andy","Emily","Trelynd"};
        assertArrayEquals(expected, Main.familyMembers());
    }

    @Test
    //Test to compare two values and pass if not the same.
    public void testFamilyGender(){
        assertNotSame(Main.familyGender("Kayte"), Main.familyGender("Andy"));
    }

    @Test
    //Test to verify that there is a value and it is not null.
    public void testFamilyAge(){
        assertNotNull(Main.familyAge("Joseph"));
    }

    @Test
    //Test to compare two values and pass if they are the same.
    public void testFamilyCity(){
        assertSame(Main.familyCity("Jenny"), Main.familyCity("Emily"));
    }

    @Test
    //Test ot verify there is not a value.
    public void testFamilyColor(){
        assertNull(Main.familyColor("Kayte"));
    }

    @Test
    //Test to confirm method ouput matches what was expected.
    public void testMultiply(){
        assertEquals(27, Main.multiply(3,9));
    }

    @Test
    //Test to confirm the result is false.
    public void testEvenNumber(){
        assertFalse(Main.evenNumber(19));
    }

    @Test
    //Test to confirm the result is true.
    public void testOddNumber(){
        assertTrue(Main.oddNumber(19));
    }
}
